#include <iostream>
#include "PointView.h"
using namespace std;



int main() {

    for(int i = 1;i<47;i++)
    {
        string filename = "/home/yanming/桌面/result/ibeo_000000000";
        filename += to_string(i);
        filename += string(".txt");
        PointView<pcl::PointXYZ> pointview;
        if(filename.substr(filename.length()-3,filename.length()) == string("pcd"))
            pointview.readFromPCD(filename);
        else
            pointview.readFromKitti(filename);
        string pcd_save_filename = "../";
        pcd_save_filename += to_string(i);
        pcd_save_filename += ".pcd";
        pointview.setPCDSaveFilename(pcd_save_filename);
        pointview.pointShow();
    }

    return 0;
}