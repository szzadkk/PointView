//
// Created by mingyan on 18-6-25.
//

#ifndef POINTSHOW_POINTVIEW_H
#define POINTSHOW_POINTVIEW_H
#include <iostream>
#include <boost/program_options.hpp>
#include <string>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/visualization/area_picking_event.h>
#include <pcl/filters/extract_indices.h>
using namespace std;


template<class PointType>
class PointView {
public:
    PointView();
    ~PointView();
    /*!
     * read from kitti raw data
     * @param filename
     * @return
     */
    bool readFromKitti(string filename);
    /*!
     * read from pcd data
     * @param filename
     * @return
     */
    bool readFromPCD(string filename);

    /*!
     * show point cloud
     */
    void pointShow();

    /*!
     * pointSelect Callback
     * @param event
     * @param args
     */
    void pointSelect_callBack(const pcl::visualization::AreaPickingEvent& event,void* args);

    /*!
     * pointPick Callback
     * @param event
     * @param args
     */
    void pointPick_callBack(const pcl::visualization::PointPickingEvent& event,void* args);

    /*!
     * keyboard Callback
     * @param event
     * @param args
     */
    void pointKeyboard_callBack(const pcl::visualization::KeyboardEvent& event, void* args);

    /*!
     * pointSave Callback
     * @param event
     * @param args
     */
    void pointSave_callBack(const pcl::visualization::KeyboardEvent& event,void* args);

    inline void setPCDSaveFilename(string& filename)
    {
        pcd_save_filename = filename;
    }



private:
    pcl::PointCloud<PointType> cloud_;
    pcl::PointCloud<PointType> cloud_select_;
    pcl::PointCloud<PointType> cloud_pick_;
    pcl::PointCloud<PointType> cloud_save_;
    pcl::visualization::PCLVisualizer viewer_;
    std::string pcd_save_filename;




};


#endif //POINTSHOW_POINTVIEW_H
