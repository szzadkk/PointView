//
// Created by mingyan on 18-6-25.
//

#include "PointView.h"

template<class PointType>
PointView<PointType>::PointView():viewer_("PointShow"){
}

template<class PointType>
PointView<PointType>::~PointView() {

}
template<class PointType>
bool PointView<PointType>::readFromKitti(string filename) {
    // load point cloud
    fstream input(filename, ios::in);
    if(!input.good()){
        cerr << "Could not read file: " <<filename<<endl;
        return false;
    }
    input.seekg(0, ios::beg);
    int i;


    for (i=0; input.good() && !input.eof(); i++) {
        PointType point;
        float data[4];
        for(int j = 0;j<4;j++)
        {
            input >> data[j];
        }
        memcpy(point.data,data,sizeof(float)*4);
        cloud_.push_back(point);
    }
    input.close();
    cout << "Read KTTI point cloud with " << i << " points"<<std::endl;
    return true;
}
template<class PointType>
bool PointView<PointType>::readFromPCD(string filename) {
    if(pcl::io::loadPCDFile(filename,cloud_) > -1)
        return true;
    return false;
}
template<class PointType>
void PointView<PointType>::pointSelect_callBack(const pcl::visualization::AreaPickingEvent &event, void *args) {
    vector<int> indices;
    if(!event.getPointsIndices(indices))
        return ;

    pcl::ExtractIndices<PointType> extract;
    extract.setInputCloud(cloud_.makeShared());
    extract.setIndices(boost::make_shared<vector<int> >(indices));
    extract.filter(cloud_select_);

    pcl::visualization::PointCloudColorHandlerGenericField<PointType> handler(" ");
    if(typeid(PointType) == typeid(pcl::PointXYZI))
        handler = pcl::visualization::PointCloudColorHandlerGenericField<PointType>(cloud_select_.makeShared(),"intensity");
    else handler = pcl::visualization::PointCloudColorHandlerGenericField<PointType>(cloud_select_.makeShared(),"x");
    viewer_.removePointCloud("cloud_select");
    viewer_.addPointCloud(cloud_select_.makeShared(),handler,"cloud_select");
    viewer_.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,10,"cloud_select");
}
template<class PointType>
void PointView<PointType>::pointPick_callBack(const pcl::visualization::PointPickingEvent &event, void *args) {
    PointType point;
    int indice = event.getPointIndex();
    point = cloud_.points[indice];
    cloud_pick_.clear();
    cloud_pick_.push_back(point);
    pcl::visualization::PointCloudColorHandlerCustom<PointType> color_handler(cloud_pick_.makeShared(),0,0,255);
    viewer_.removePointCloud("cloud_pick");
    viewer_.addPointCloud(cloud_pick_.makeShared(),color_handler,"cloud_pick");
    viewer_.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE,15,"cloud_pick");
}
template<class PointType>
void PointView<PointType>::pointKeyboard_callBack(const pcl::visualization::KeyboardEvent &event, void *args) {
    char c = event.getKeyCode();
    if(c == 'o')            //configure correct select
        cloud_save_.insert(cloud_save_.end(),cloud_select_.begin(),cloud_select_.end());

    if(c == 'p')
    {
        pcl::visualization::PCLVisualizer viewer_save("PointSelect");
        pcl::visualization::PointCloudColorHandlerCustom<PointType> handler(cloud_save_.makeShared(),0,170,255);
        viewer_save.addPointCloud(cloud_save_.makeShared(),handler,"cloud_save");
        viewer_save.registerKeyboardCallback(&PointView::pointSave_callBack,*this,(void*)&cloud_save_);
        viewer_save.spin();
        viewer_save.close();
    }
}
template<class PointType>
void PointView<PointType>::pointSave_callBack(const pcl::visualization::KeyboardEvent &event, void *args) {
    pcl::PointCloud<PointType>* cloud_save = reinterpret_cast<pcl::PointCloud<PointType>*>(args);
    if(event.getKeyCode() == 'm')
        pcl::io::savePCDFileASCII(pcd_save_filename,*cloud_save);
}
template<class PointType>
void PointView<PointType>::pointShow() {
    pcl::visualization::PointCloudColorHandlerGenericField<PointType> handler(" ");
    if(typeid(PointType) == typeid(pcl::PointXYZI))
        handler = pcl::visualization::PointCloudColorHandlerGenericField<PointType>(cloud_.makeShared(),"intensity");
    else handler = pcl::visualization::PointCloudColorHandlerGenericField<PointType>(cloud_.makeShared(),"x");
    viewer_.addPointCloud<PointType>(cloud_.makeShared(),handler,"cloud");
    viewer_.registerAreaPickingCallback(&PointView<PointType>::pointSelect_callBack,*this);
    viewer_.registerPointPickingCallback(&PointView<PointType>::pointPick_callBack,*this);
    viewer_.registerKeyboardCallback(&PointView<PointType>::pointKeyboard_callBack,*this);
    viewer_.spin();
    viewer_.close();

}
template class PointView<pcl::PointXYZ>;