cmake_minimum_required(VERSION 3.5)
project(pointshow)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_BUILD_TYPE Release)
find_package(PCL REQUIRED)
include_directories(${PCL_INCLUDE_DIRS})


add_executable(pointshow main.cpp PointView.cpp PointView.h)
target_link_libraries(pointshow ${PCL_LIBRARIES})